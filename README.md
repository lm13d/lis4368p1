> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Lindsey Mabrey

### Project 1 Requirements:

*Sub-Heading:*

1. Clone starter files and review subdirectories/files.
2. Open index.jsp and review/modify code
3. Perform form validation
4. Push from remote repo

#### README.md file should include the following items:

* Screenshot of form validation


#### Project 1 Screenshots:

*Screenshot of running Form Validation*:

![Successful Form Validation](img/validation.png)


